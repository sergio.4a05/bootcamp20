package com.bootcampjava.event.model;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PersonaModel {
	
@JsonProperty("PersonaId")	
	
	@Builder.Default
	private long id =0;
@Builder.Default
	private String nombres = null;
@Builder.Default
	private String apellidos= null;
@Builder.Default
	private String dni= null;
@Builder.Default
	private String correo= null;
@Builder.Default
	private long id_afp= 0;	
	
}
