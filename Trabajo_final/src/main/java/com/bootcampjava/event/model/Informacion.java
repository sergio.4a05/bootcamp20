package com.bootcampjava.event.model;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Informacion {
	
	@JsonProperty("InfoId")
	
	@Builder.Default
	private Long id = null;
	
	@Builder.Default
	private String dni= null;
	
	@Builder.Default
	private Double monto_dis= null;
	
	@Builder.Default
	private String fecha_retiro= null;
}



