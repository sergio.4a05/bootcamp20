package com.bootcampjava.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bootcampjava.event.domain.Solicitud;
public interface RepositorySolicitud extends JpaRepository <Solicitud, Long> {

}