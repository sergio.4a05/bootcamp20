package com.bootcampjava.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bootcampjava.event.domain.Persona;
public interface RepositoryPersona extends JpaRepository <Persona, Long> {

}
