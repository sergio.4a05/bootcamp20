package com.bootcampjava.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bootcampjava.event.domain.Informacion;
public interface RepositoryInformacion extends JpaRepository <Informacion, Long> {

}