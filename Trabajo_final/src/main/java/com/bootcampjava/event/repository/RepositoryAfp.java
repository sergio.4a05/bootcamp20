package com.bootcampjava.event.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bootcampjava.event.domain.Afp;
public interface RepositoryAfp extends JpaRepository <Afp, Long> {

}
