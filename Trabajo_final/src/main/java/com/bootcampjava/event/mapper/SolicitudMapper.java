package com.bootcampjava.event.mapper;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import com.bootcampjava.event.domain.Solicitud;
import com.bootcampjava.event.model.SolicitudModel;

@Mapper(componentModel = "spring")
public interface SolicitudMapper {
		
	Solicitud solicitudModelToSolicitud (SolicitudModel model);	 
	SolicitudModel solicitudToSolicitudModel (SolicitudModel solicitud);	 
	SolicitudModel solicitudToSolicitudModels(List<Solicitud> solicituds);
	 
	 void update(@MappingTarget Solicitud entity, SolicitudModel updateEntity);
	SolicitudModel solicitudToSolicitudModel(Solicitud solicitud);

}
