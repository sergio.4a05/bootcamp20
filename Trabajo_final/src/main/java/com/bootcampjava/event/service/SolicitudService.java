package com.bootcampjava.event.service;
import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bootcampjava.event.domain.Solicitud;
import com.bootcampjava.event.mapper.SolicitudMapper;
import com.bootcampjava.event.model.SolicitudModel;
import com.bootcampjava.event.repository.RepositorySolicitud;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SolicitudService {

	private final RepositorySolicitud solicitudRepository;
	private final SolicitudMapper solicitudMapper;

	public List<SolicitudModel> findAll()  throws Exception {
		List<Solicitud> solicituds = solicitudRepository.findAll();
		return solicitudMapper.solicitudToSolicitudModels(solicituds);
	}

	 
	public SolicitudModel findById(Long id) throws Exception {
		Optional<Solicitud> Solicitud = solicitudRepository.findById(id);
		if(Solicitud.isPresent())	return solicitudMapper.solicitudToSolicitudModel(Solicitud.get()); 
		else throw new Exception("No se encontraron datos");
	}

	
	public SolicitudModel create(SolicitudModel solicitudModel)  throws Exception{
		Solicitud solicitud = solicitudRepository.save(solicitudMapper.solicitudModelToSolicitud(solicitudModel);
		return solicitudMapper.solicitudModelToSolicitud(solicitud);
	}

	
	public void update(Long id, SolicitudModel eventModel)  throws Exception {
		Optional<Solicitud> eventOptional = solicitudRepository.findById(id);
		
		if(eventOptional.isPresent()) {
			Solicitud eventToUpdate = eventOptional.get();
			solicitudMapper.update(eventToUpdate, eventModel);
			solicitudRepository.save(eventToUpdate);
		}
		else throw new Exception("No se encontraron datos");
	}

	
	public void deleteById(Long id)  throws Exception {
		solicitudRepository.deleteById(id);
	}

}
