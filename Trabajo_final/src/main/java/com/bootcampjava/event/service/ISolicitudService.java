package com.bootcampjava.event.service;
import java.util.List;

import com.bootcampjava.event.model.SolicitudModel;
public interface ISolicitudService {
	List<SolicitudModel> findAll() throws Exception;
	
	SolicitudModel findById(Long id) throws Exception;

	SolicitudModel create(SolicitudModel solicitudModel) throws Exception;

	void update(Long id, SolicitudModel solicitudModel) throws Exception;

	void deleteById(Long id) throws Exception;
}
