package com.bootcampjava.event.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder 
@NoArgsConstructor
@AllArgsConstructor
//Creando la entidad PERSONA
public class Persona {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String nombres;
	private String apellidos;
	private String dni;
	private String correo;
	private long id_afp;	
}
