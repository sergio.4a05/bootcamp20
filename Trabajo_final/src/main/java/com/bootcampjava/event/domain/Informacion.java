package com.bootcampjava.event.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@Builder 
@NoArgsConstructor
@AllArgsConstructor
//Creando la entidad INFORMACION
public class Informacion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id;
		private String dni;
		private Double monto_dis;
		private String fecha_retiro;
}
